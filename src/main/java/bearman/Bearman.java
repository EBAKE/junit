package bearman;

import food.Food;

public class Bearman {
	
	/**
	 * Creates a bearman, and initializes him to hungry
	 */
	public Bearman() {
		hungry = true;
	}
	
	public void eat(Food theFood) {
		theFood.feedBearman(this);
	}
	
	private boolean hungry;

	public boolean isHungry() {
		return hungry;
	}

	public void setHungry(boolean hungry) {
		this.hungry = hungry;
	}
	
}
